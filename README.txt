README.txt:

The name of the file for the AI is aport (not aport2 or player)


If am solo, so I did did all programming, etc...

For the initial design, I got basic program functionality running by initializing
the board and taking the first possible move found. I used this to find any bugs
in handling moves (i.e. I initially declared moves as move(y,x), not move(x,y)).

Once I was sure the AI board was the same as the game board, I started making a
heuristic for the AI. Clearly, different board positions have different 
importance. Thus, the weights I use for each position are:

Weight layout:				Reasoning:

a b c d d c b a		a = 20		- corners can't be captured -> high positive
b e f g g f e b		b = -4		- pieces immediately adjacent to the corners give enemy access
c f h h h h f c		c = 12		  to the corners -> high negative
d g h i i h g d		d = 8		- edge pieces better than normal pieces (harder to capture)
d g h i i h g d		e = -7		- middle pieces are benficial, but ownership is volitile
c f h h h h f c		f = -5
b e f g g f e b		g = 1
a b c d d c b a		h = 2
			i = -3

In addition, I wrote a function in board.cpp that returns 1 if the given position
belongs to aport, 0 if unoccupied and -1 if controlled by the enemy. In order calculate
the board heuristic, I did componentwise multiplication of the two matrices and took the sum
of these results. Thus, allowing the enemy access to a corner lowers the score for that board.
The result of this value will be a number (pos) approimately between -100 and 100.

However, these positions only constitute a small portion of over heuristic. I also consider the
difference in number of pieces for each player (diff), the number of corners each player has
(cor), the number of pieces each has adjacent to a corner (adj), and the difference in the
number of moves each player has on that board (mob). All of these are normalized to be values
between -100 and 100. The final value returned is 5*diff + 60*cor + 15*adj + 15*mob + 5*pos.
The overall strategy of aport is to limit the number of availible moves the enemy has and then
force it into taking pieces right next to the corners, thus given the corners to aport.


Next, I had to construct the game tree. For this, Implimented alpha-beta trimming. The variable
"depth" in the beginning of the program defines how many  moves into the future
to look. The default setting for depth is 8, but if the game is about to end, the algorithm changes
to depth a lower depth to avoid running out of time (like using iterative deepening).
