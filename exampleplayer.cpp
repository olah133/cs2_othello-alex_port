#include "exampleplayer.h"
#include <stdio.h>
Board board;
int * weights = new int[64];
Side me;
Side you;
int depth = 8;
int progress = 0;

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 *
 * Creates the list used as matrix in calculating part of the heuristic.
 * Also initializes the main board, named "board".
 */
ExamplePlayer::ExamplePlayer(Side side) {
    me = side;
    if(me == BLACK) {
        you = WHITE;
    }
    else {
        you = BLACK;
    }
    int corner = 20;
    int adj1 = -4;
    int adj2 = -5;
    int diag = -7;
    int edge1 = 12;
    int edge2 = 8;
    int mid1 = 1;
    int mid2 = 2;
    int center = -3;
    for(int i = 0; i < 4; i++) {
        for(int j = 0; j < 4; j++) {
	    if(i == 0 && j == 0)
	        weights[8*i + j] = corner;
	    else if((i == 0 && j == 1) || (i == 1 && j == 0))
	        weights[8*i + j] = adj1;
	    else if((i == 0 && j == 2) || (i == 2 && j == 0))
	        weights[8*i + j] = edge1;
	    else if((i == 0 && j == 3) || (i == 3 && j == 0))
	        weights[8*i + j] = edge2;
	    else if(i == 1 && j == 1)
	        weights[8*i + j] = diag;
	    else if((i == 1 && j == 2) || (i == 2 && j == 1))
	        weights[8*i + j] = adj2;
	    else if((i == 1 && j == 3) || (i == 3 && j == 1))
	        weights[8*i + j] = mid1;
	    else if((i == 2 && j == 2) || (i == 2 && j == 3) || (i == 3 && j == 2))
	        weights[8*i + j] = mid2;
	    else
	        weights[8*i + j] = center;
	    weights[8*(7-i) + (7-j)] = weights[8*i + j];
	    weights[8*(7-i) + j] = weights[8*i + j];
	    weights[8*i + (7-j)] = weights[8*i + j];
	}
    }
}

/*
 * Destructor for the player.
 */
ExamplePlayer::~ExamplePlayer() {
}


/*
 * Compute the next move given the opponent's last move. Each AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * If there are no valid moves for your side, doMove must return NULL.
 *
 * Important: doMove must take no longer than the timeout passed in 
 * msLeft, or your AI will lose! The move returned must also be legal.
 */
Move *ExamplePlayer::doMove(Move *opponentsMove, int msLeft) {
    if(msLeft < 30000 && msLeft != 0)
        depth = 5;
    else if(msLeft < 60000 && msLeft != 0)
        depth = 6;
    else if(msLeft < 180000 && msLeft != 0)
        depth = 7;
    progress += 2;
    board.doMove(opponentsMove, you);
    if(board.hasMoves(me)) {
        Board b = board;
        int * found  = iter(b, me, depth, -10000000, 10000000);
	Move * best = new Move(found[1], found[2]);
	board.doMove(best, me);
	return best;
    }
    return NULL;
}

/*
 * Does one hypothetical move to determine the best future game state
 * Passes the board state b, alpha-beta values and remaining moves to go.
 * Returns three integers stored in triple.
 * triple[0] = heuristic of best board found in this set of nodes
 * triple[1] = coordinate of x-move required to get the best board
 * triple[2] = coordinate of y-move required to get the best board
 */
int * iter(Board b, Side one, int count, int alpha, int beta) {
    count -= 1;
    int * triple = new int[3];
    int * first = firstMove(b, one);
    triple[1] = first[0];
    triple[2] = first[1];
    if(one == me) {
	triple[0] = -10000000;
    }
    else {
        triple[0] = 10000000;
    }
    Side other;
    if(one == BLACK) {
        other = WHITE;
    }
    else {
        other = BLACK;
    }
 
    for(int i = 0; i < 8; i++) {
        for(int j = 0; j < 8; j++) {
	    // stop searching if either player makes a mistake (i.e beta <= alpha)
	    if(beta > alpha) {
	        Move move(j,i);
		if(b.checkMove(&move, one)) {
		    Board b2 = b;
		    b2.doMove(&move, one);
		    // determine if current node is a leaf
		    if((count == 0) || !b2.hasMoves(other)) {
		        int temp = boardState(b2);
			if(one == me) {
			    // if current player is aport
			    if(temp > triple[0]) {
			        alpha = temp;
				triple[0] = temp;
				triple[1] = j;
				triple[2] = i;
			    }
			}
			else {
			    // if current player is not aport
			    if(temp < triple[0]) {
			        beta = temp;
				triple[0] = temp;
				triple[1] = j;
				triple[2] = i;
			    }
			}
		    }
		    else {
		        if(one == me) {
			    // if current player is aport
			    if(triple[0] < beta) {
			      int * temp2 = iter(b2, other, count, alpha, beta);
				if(temp2[0] > triple[0]) {
				    alpha = temp2[0];
				    triple[0] = temp2[0];
				    triple[1] = j;
				    triple[2] = i;
				}
			    }
			}
			else {
			    // if current player is not aport
			    if(triple[0] > alpha) {
			      int * temp2 = iter(b2, other, count, alpha, beta);
				if(temp2[0] < triple[0]) {
				    beta = temp2[0];
				    triple[0] = temp2[0];
				    triple[1] = j;
				    triple[2] = i;
				}
			    }
			}
		    }
		}
	    }
	}
    }
    return triple;
}


/*
 * Calculates the heuristic value for the given board
 */
int boardState(Board b) {
    int diff, cor, adj, mob, pos, n, u;
    n = b.count(me);
    u = b.count(you);
    diff = 100 * (n - u) / (n + u);
    cor = 25 * b.countCorners(me);
    adj = -12.5 * b.countAdj(me);
    n = b.numMoves(me);
    u = b.numMoves(you);
    if(n + u != 0)
        mob = 100 * (n - u) / (n + u);
    else
        mob = 0;
    pos = 0;
    for(int i = 0; i < 8; i++) {
        for(int j = 0; j < 8; j++) {
	    pos += weights[8*i + j] * b.status(j, i, me);
	}
    }
    return 5*diff + 60*cor + 15*adj + 15*mob + 5*pos;
}

/*
 * Returns the coordinates of the first possible move at depth 1
 * to avoid choosing an invalid move.
 */

int * firstMove(Board b, Side one) {
    int * temp = new int[2];
    for(int i = 0; i < 8; i++) {
        for(int j = 0; j < 8; j++) {
	    Move move(j,i);
	    if(b.checkMove(&move, one)) {
	        temp[0] = j;
		temp[1] = i;
		return temp;
	    }
        }
    }
    return temp;
}

